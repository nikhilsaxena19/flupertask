package com.flupertask.adapter;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.flupertask.R;

import java.util.List;


public class ColorRecyclerViewAdapter extends RecyclerView.Adapter<ColorRecyclerViewAdapter.CustomViewHolder> {

    private String[] colors;

    private OnItemClickListener listener;

    public ColorRecyclerViewAdapter(String[] colors,OnItemClickListener listener) {

        this.colors = colors;
        this.listener = listener;
    }



    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.color_view_item, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final CustomViewHolder holder, final int position) {

        boolean isSelected = false;

        holder.image.setBackgroundColor(Color.parseColor(getItem(position))); // for setting current color as image background
        int strokeWidth = 1;
        int strokeColor = Color.parseColor("#000000");
        int fillColor = Color.parseColor(colors[position]);
        GradientDrawable gD = new GradientDrawable();
        gD.setColor(fillColor);
        gD.setShape(GradientDrawable.OVAL);
        gD.setStroke(strokeWidth, strokeColor);
        holder.image.setBackground(gD);
        final boolean[] finalIsSelected = {isSelected};  // comvet into final one element array
        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!finalIsSelected[0]) {
                    finalIsSelected[0] = true;
                    holder.linear.setBackgroundColor(v.getContext().getColor(R.color.grey)); // change background when select
                    listener.onItemClick(colors[position], finalIsSelected[0]); // call listener and pass current color and selection status
                } else {

                    finalIsSelected[0] = false;
                    holder.linear.setBackgroundColor(v.getContext().getColor(R.color.white));
                    listener.onItemClick(colors[position], finalIsSelected[0]);
                }
            }
        });




    }

    @Override
    public int getItemCount() {
        return colors.length; // return size of color
    }

    public String getItem(int position) {
        return colors[position];// returns current color
    }


    protected class CustomViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout linear; // for changing background
        private ImageView image; // for displaying color as imageview


        public CustomViewHolder(View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.image);
            linear = itemView.findViewById(R.id.linear);


        }
    }

    public interface OnItemClickListener {
        void onItemClick(String item, boolean checked);
    }

}
