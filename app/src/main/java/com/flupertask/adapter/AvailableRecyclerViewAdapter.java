package com.flupertask.adapter;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.flupertask.R;

import java.util.List;


public class AvailableRecyclerViewAdapter extends RecyclerView.Adapter<AvailableRecyclerViewAdapter.CustomViewHolder> {

    List<Integer> color;

    public AvailableRecyclerViewAdapter(List<Integer> color) {

        this.color = color;
    }



    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.color_view_item, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final CustomViewHolder holder, final int position) {


        holder.image.setBackgroundColor(getItem(position)); // for setting current color as image background
        int strokeWidth = 1;
        int strokeColor = Color.parseColor("#000000");
        int fillColor = color.get(position);
        GradientDrawable gD = new GradientDrawable();
        gD.setColor(fillColor);
        gD.setShape(GradientDrawable.OVAL);
        gD.setStroke(strokeWidth, strokeColor);
        holder.image.setBackground(gD);






    }

    @Override
    public int getItemCount() {
        return color.size(); // return size of color
    }

    public int getItem(int position) {
        return color.get(position);// returns current color
    }


    protected class CustomViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout linear; // for changing background
        private ImageView image; // for displaying color as imageview


        public CustomViewHolder(View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.image);
            linear = itemView.findViewById(R.id.linear);


        }
    }

    public interface OnItemClickListener {
        void onItemClick(String item, boolean checked);
    }

}
