package com.flupertask.adapter;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.PointerIcon;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flupertask.R;
import com.flupertask.model.Product;
import com.flupertask.ui.FullImageCustomDialog;
import com.flupertask.ui.MainActivity;
import com.flupertask.ui.UpdateProductActivity;
import com.flupertask.utils.Constants;
import com.flupertask.utils.DataConverter;

import java.io.Serializable;
import java.util.List;

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ProductViewHolder> {

    public interface OnDeleteClickListener {
        void OnDeleteClickListener(Product product); // for deleting any Product
    }

    private final LayoutInflater layoutInflater;
    private Context mContext;
    private FragmentManager fragmentManager;
    private List<Product> mProducts;
    private OnDeleteClickListener onDeleteClickListener;

    public ProductListAdapter(Context context, OnDeleteClickListener listener, FragmentManager fragmentManager) {
        layoutInflater = LayoutInflater.from(context);
        mContext = context;
        this.fragmentManager = fragmentManager; // fragment manageer for launching dialog fragment
        this.onDeleteClickListener = listener; // listener for delete product
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = layoutInflater.inflate(R.layout.product_list_item, parent, false);
        ProductViewHolder viewHolder = new ProductViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {

        if (mProducts != null) {
            Product product = mProducts.get(position);
            holder.setData(product, position); // set current item data in holder
            holder.setListeners(product);
        }
    }

    @Override
    public int getItemCount() {
        if (mProducts != null)
            return mProducts.size(); // returns current size of list
        else return 0;
    }

    public void setProducts(List<Product> products) {
        mProducts = products;
        notifyDataSetChanged();
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder {

        private TextView productNameView, productRegularPriceView, productSalePriceView;
        private int mPosition;

        private ImageView imgDelete, imgEdit,imageView;

        public ProductViewHolder(View itemView) {
            super(itemView);
            productNameView = itemView.findViewById(R.id.product_name);
            productRegularPriceView = itemView.findViewById(R.id.product_regular_price);
            productSalePriceView = itemView.findViewById(R.id.product_sale_price);
            imgDelete = itemView.findViewById(R.id.ivRowDelete);
            imgEdit = itemView.findViewById(R.id.ivRowEdit);
            imageView = itemView.findViewById(R.id.imageView);
        }

        public void setData(Product product, int position) { // setting all holder view data
            productNameView.setText("Name:"+product.getName());
            productSalePriceView.setText("Sale Price:"+product.getSalePrice());
            productRegularPriceView.setText("Reg. Price:"+product.getRegularPrice());
            imageView.setImageBitmap(DataConverter.convertByteArray2Image(product.getProductImage()));
            mPosition = position; // asssing current position to mposition variable
        }

        public void setListeners(final Product product) {


            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // for launching a full screen dialog fragment for displaying image
                    FullImageCustomDialog.image =   mProducts.get(mPosition).getProductImage();
                    FullImageCustomDialog fullImageCustomDialog = new FullImageCustomDialog();
                    fullImageCustomDialog.show(fragmentManager, "Full Image Dialog");
                }
            });
            imgEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // for updating any product
                    Intent intent = new Intent(mContext, UpdateProductActivity.class);
                    intent.putExtra(Constants.PRODUCT_ID, mProducts.get(mPosition).getId());
                    ((Activity) mContext).startActivityForResult(intent, MainActivity.UPDATE_NOTE_ACTIVITY_REQUEST_CODE);
                }
            });

            imgDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // for deleting any product from database
                    if (onDeleteClickListener != null) {
                        onDeleteClickListener.OnDeleteClickListener(mProducts.get(mPosition));

                    }
                }
            });
        }
    }
}
