package com.flupertask.utils;

public class Constants {
    public static final String PRODUCT_ID = "product_id";
    public static final String PRODUCT_NAME = "product_name";
    public static final String PRODUCT_REG_PRICE = "product_reg_price";
    public static final String PRODUCT_SALE_PRICE = "product_sale_price";
    public static final String PRODUCT_IMAGE ="product_image";
    public static final String PRODUCT_DESC = "product_desc";
    public static final String PRODUCT_COLORS = "product_colors";
    public static final String INTENT_PRODUCT = "intent_product";




}
