package com.flupertask.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.List;

@Entity(tableName = "products")
public class Product {

    @PrimaryKey(autoGenerate = true) // defining id as primary key
    @NonNull
    private int id;

    @NonNull
    @ColumnInfo(name = "product_name")
    private String name;


    @NonNull
    @ColumnInfo(name = "product_description")
    private String description;

    @NonNull
    @ColumnInfo(name = "product_reg_price")
    private String regularPrice;

    @NonNull
    @ColumnInfo(name = "color")
    private List<Integer> color;

    @NonNull
    @ColumnInfo(name = "product_sale_price")
    private String salePrice;

    @ColumnInfo(typeAffinity = ColumnInfo.BLOB)
    private byte[] productImage;


    public int getId() {
        return id;
    }

    @NonNull
    public String getName() {
        return name;
    }

    @NonNull
    public String getDescription() {
        return description;
    }

    @NonNull
    public String getRegularPrice() {
        return regularPrice;
    }

    @NonNull
    public String getSalePrice() {
        return salePrice;
    }

    @NonNull
    public List<Integer> getColor() {
        return color;
    }

    public void setColor(@NonNull List<Integer> color) {
        this.color = color;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    public void setDescription(@NonNull String description) {
        this.description = description;
    }

    public void setRegularPrice(@NonNull String regularPrice) {
        this.regularPrice = regularPrice;
    }

    public void setSalePrice(@NonNull String salePrice) {
        this.salePrice = salePrice;
    }

    public byte[] getProductImage() {
        return productImage;
    }

    public void setProductImage(byte[] productImage) {
        this.productImage = productImage;
    }

}
