package com.flupertask.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;
import android.util.Log;

import com.flupertask.model.Product;
import com.flupertask.room.ProductDao;
import com.flupertask.room.ProductRoomDatabase;

public class UpdateProductViewModel extends AndroidViewModel {

    private String TAG = this.getClass().getSimpleName();
    private ProductDao productDao;
    private ProductRoomDatabase db;

    public UpdateProductViewModel(@NonNull Application application) {
        super(application);
        Log.i(TAG, "Edit ViewModel");
        db = ProductRoomDatabase.getDatabase(application);
        productDao = db.productDao();
    }




    public LiveData<Product> getProduct(int productId) {
        return productDao.getProduct(productId);
    }
}
