package com.flupertask.room;

import android.arch.persistence.room.TypeConverter;
import android.arch.persistence.room.TypeConverters;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class RoomTypeConverters {
    @TypeConverter
    public static String fromList(List<Integer> list) {
        return new Gson().toJson(list); // get each color by using Gson
    }

    @TypeConverter
    public static List<Integer> fromJson(String value) {
        Type listType = new TypeToken<List<Integer>>() {
        }.getType();
        return new Gson().fromJson(String.valueOf(value), listType);
    }
}
