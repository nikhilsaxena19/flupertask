package com.flupertask.room;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.flupertask.model.Product;

import java.util.List;

@Dao
public interface ProductDao {

    @Insert
    void insert(Product product); // insert product in database

    @Query("SELECT * FROM products")
    LiveData<List<Product>> getAllProducts(); // get all  products from  database

    @Query("SELECT * FROM products WHERE id=:product_id")
    LiveData<Product> getProduct(int product_id); // get all  products from  database

    @Update
    void update(Product product); // for updaing product

    @Delete
    int delete(Product product); // for deleting product
}
