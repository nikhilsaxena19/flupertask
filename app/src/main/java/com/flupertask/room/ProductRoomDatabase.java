package com.flupertask.room;


import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;

import com.flupertask.model.Product;


@Database(entities = Product.class, version = 1, exportSchema = false)
@TypeConverters(RoomTypeConverters.class)
public abstract class ProductRoomDatabase extends RoomDatabase {

    public abstract ProductDao productDao();

    private static volatile ProductRoomDatabase productRoomDatabase;

    public static ProductRoomDatabase getDatabase(final Context context) {
        if (productRoomDatabase == null) {
            synchronized (ProductRoomDatabase.class) {
                if (productRoomDatabase == null) {
                    productRoomDatabase = Room.databaseBuilder(context.getApplicationContext(),
                            ProductRoomDatabase.class, "product_database")
                            .build();
                }
            }
        }
        return productRoomDatabase;
    }
}
