package com.flupertask.ui;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.flupertask.R;
import com.flupertask.utils.DataConverter;

public class FullImageCustomDialog extends DialogFragment {

    private ImageView imageView;
    public static byte [] image; // for storing selected image in this variable

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.full_image_view, container, false);
        imageView = (ImageView) view.findViewById(R.id.imageView);
        imageView.setImageBitmap(DataConverter.convertByteArray2Image(image)); // covert byte to bitmap

        return view;
    }


}