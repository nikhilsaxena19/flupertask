package com.flupertask.ui;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.flupertask.R;
import com.flupertask.adapter.ColorRecyclerViewAdapter;
import com.flupertask.utils.Constants;
import com.flupertask.utils.DataConverter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class AddProductActivity extends AppCompatActivity {
    private TextInputLayout product_name, product_regular_price, product_sale_price, product_desc;
    private Button add_product;
    private int gallery_Code = 20;
    private Bitmap mBitmap;
    private Button upload_image;
    private ImageView imageView;
    private RecyclerView color_recyclerView;
    private String[] allColors;
    private ArrayList<String> selectedColorsList;
    private int MY_GALLERY_PERMISSION_CODE = 1001;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);
        viewInitialization();
        upload_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_GALLERY_PERMISSION_CODE);
                } else {
                    openGallery(); // for open gallery to add image
                }

            }
        });

        add_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent resultIntent = new Intent();


                if (checkValidation()) { // checking validation

                    resultIntent.putExtra(Constants.PRODUCT_NAME, product_name.getEditText().getText().toString().trim()); // for product name
                    resultIntent.putExtra(Constants.PRODUCT_DESC, product_desc.getEditText().getText().toString().trim()); // for product description
                    resultIntent.putExtra(Constants.PRODUCT_REG_PRICE, product_regular_price.getEditText().getText().toString().trim());// for Regular price
                    resultIntent.putExtra(Constants.PRODUCT_SALE_PRICE, product_sale_price.getEditText().getText().toString().trim());// for sale price
                    resultIntent.putExtra(Constants.PRODUCT_IMAGE, DataConverter.convertImage2ByteArray(mBitmap));// for product image
                    resultIntent.putStringArrayListExtra(Constants.PRODUCT_COLORS, selectedColorsList);// for product multiple colors

                    setResult(RESULT_OK, resultIntent);
                    finish();
                }


            }
        });

        setActionbar(); // setting action bar title

        color_recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        allColors = getResources().getStringArray(R.array.colors); // getting all predefined colors
        selectedColorsList = new ArrayList<>();
        color_recyclerView.setAdapter(new ColorRecyclerViewAdapter(allColors, new ColorRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String item, boolean checked) {

                if (checked) { // for checking color is selected or not.
                    if (!selectedColorsList.contains(item)) {
                        selectedColorsList.add(item); // Storing selected colors in another arraylist
                    }
                } else {

                    if (selectedColorsList.contains(item)) {
                        selectedColorsList.remove(item); // removing deselected colors from same arraylist

                    }
                }


            }
        }));
    }


    private void openGallery() {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto, gallery_Code); // passing request for onactivity result callback
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == gallery_Code) {
            if (resultCode == RESULT_OK) {
                Uri selectedImage = data.getData();
                File imageFile = new File(getRealPathFromURI(selectedImage)); // get imaeg current path from uri
                mBitmap = BitmapFactory.decodeFile(imageFile.getPath());
                if (mBitmap != null) {

                    imageView.setVisibility(View.VISIBLE);
                    imageView.setImageBitmap(mBitmap);
                } else {
                    imageView.setVisibility(View.GONE);
                    upload_image.setVisibility(View.VISIBLE);
                }
            } else {
                imageView.setVisibility(View.GONE);
                upload_image.setVisibility(View.VISIBLE);

            }

        }
    }

    private void setActionbar() {

        getSupportActionBar().setDisplayHomeAsUpEnabled(true); // for displaying actionbar back button
        getSupportActionBar().setTitle("Create Product");


    }

    @Override
    public boolean onSupportNavigateUp() { // for handle back button in actionbar
        onBackPressed();
        return super.onSupportNavigateUp();

    }

    private void showSnackBar(View view, String message) {

        Snackbar.make(view, message, Snackbar.LENGTH_SHORT).show();
    }

    private boolean checkValidation() {
        boolean isValidate = false;
        if (product_name.getEditText().getText().toString().equals("")) {
            isValidate = false;
            showSnackBar(product_name, "Please Enter Product Name");
        } else if (product_regular_price.getEditText().getText().toString().equals("")) {
            isValidate = false;
            showSnackBar(product_name, "Please Enter Regular Price");
        } else if (product_sale_price.getEditText().getText().toString().equals("")) {
            isValidate = false;
            showSnackBar(product_name, "Please Enter Sale Price");
        } else if (product_desc.getEditText().getText().toString().equals("")) {
            isValidate = false;
            showSnackBar(product_name, "Please Enter Production description");
        } else if (mBitmap == null) {
            isValidate = false;
            showSnackBar(product_name, "Please Select image");
        } else if (selectedColorsList.size() == 0) {
            isValidate = false;
            showSnackBar(product_name, "Please Select atleast one Color.");
        } else {

            isValidate = true;
        }

        return isValidate;
    }

    private void viewInitialization() {
        // all views initialization here

        imageView = (ImageView) findViewById(R.id.imageView);
        upload_image = (Button) findViewById(R.id.upload_image);
        add_product = (Button) findViewById(R.id.add_product);
        product_name = (TextInputLayout) findViewById(R.id.product_name);
        product_regular_price = (TextInputLayout) findViewById(R.id.product_regular_price);
        product_sale_price = (TextInputLayout) findViewById(R.id.product_sale_price);
        product_desc = (TextInputLayout) findViewById(R.id.product_desc);
        color_recyclerView = (RecyclerView) findViewById(R.id.color_recyclerView);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_GALLERY_PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                openGallery();
            } else {
                Toast.makeText(this, "Storage permission denied", Toast.LENGTH_LONG).show();
            }
        }
    }
}
