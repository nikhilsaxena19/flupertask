package com.flupertask.ui;

import android.Manifest;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.flupertask.R;
import com.flupertask.adapter.AvailableRecyclerViewAdapter;
import com.flupertask.model.Product;
import com.flupertask.utils.Constants;
import com.flupertask.utils.DataConverter;
import com.flupertask.viewmodel.UpdateProductViewModel;
import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class UpdateProductActivity extends AppCompatActivity {
    private Bundle bundle;
    private int productId;
    private LiveData<Product> productLiveData;
    private TextInputLayout product_name, product_regular_price, product_sale_price, product_desc;
    private Button update_product;
    private Button upload_image;
    private ImageView imageView;
    private int gallery_Code = 20;
    private Bitmap mBitmap;
    private UpdateProductViewModel updateProductViewModel;
    private RecyclerView color_recyclerView;
    ArrayList<String> selectedColorsList;
    private int MY_GALLERY_PERMISSION_CODE = 1001;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_product);

        initializeViews(); //initialization of views
        upload_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_GALLERY_PERMISSION_CODE);
                } else {
                    openGallery(); // open gallery after permission is enabled
                }
            }
        });


        productLiveData.observe(this, new Observer<Product>() {
            @Override
            public void onChanged(@Nullable Product product) {
                product_name.getEditText().setText(product.getName());
                product_desc.getEditText().setText(product.getDescription());
                product_regular_price.getEditText().setText(product.getRegularPrice());
                product_sale_price.getEditText().setText(product.getSalePrice());
                mBitmap = DataConverter.convertByteArray2Image(product.getProductImage());// convert byte array to bitmap
                imageView.setVisibility(View.VISIBLE);
                imageView.setImageBitmap(mBitmap);
                setAvailableColors(product.getColor()); // set available colors

            }
        });


        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // for displaying image in full screen
                FullImageCustomDialog.image = DataConverter.convertImage2ByteArray(mBitmap);
                FullImageCustomDialog fullImageCustomDialog = new FullImageCustomDialog();
                fullImageCustomDialog.show(getFragmentManager(), "Full Image Dialog");
            }
        });
        update_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // we can update our product from here
                updateProduct();
            }
        });


        setActionbar();// set action bar title
    }

    public void updateProduct() {

        if (checkValidation()) {
            Intent resultIntent = new Intent();
            resultIntent.putExtra(Constants.PRODUCT_ID, productId + "");
            resultIntent.putExtra(Constants.PRODUCT_NAME, product_name.getEditText().getText().toString().trim());
            resultIntent.putExtra(Constants.PRODUCT_DESC, product_desc.getEditText().getText().toString().trim());
            resultIntent.putExtra(Constants.PRODUCT_REG_PRICE, product_regular_price.getEditText().getText().toString().trim());
            resultIntent.putExtra(Constants.PRODUCT_SALE_PRICE, product_sale_price.getEditText().getText().toString().trim());
            resultIntent.putExtra(Constants.PRODUCT_IMAGE, DataConverter.convertImage2ByteArray(mBitmap));
            resultIntent.putExtra(Constants.PRODUCT_COLORS, selectedColorsList);


            setResult(RESULT_OK, resultIntent);// set all data again in intent.
            finish();
        }
    }

    private void openGallery() {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto, gallery_Code);
    }

    private String getRealPathFromURI(Uri contentURI) { // get actual path of image from uri
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == gallery_Code) {
            if (resultCode == RESULT_OK) {
                Uri selectedImage = data.getData(); // get image uri here
                File imageFile = new File(getRealPathFromURI(selectedImage));
                mBitmap = BitmapFactory.decodeFile(imageFile.getPath());
                if (mBitmap != null) {

                    imageView.setVisibility(View.VISIBLE);
                    imageView.setImageBitmap(mBitmap);
                } else {
                    imageView.setVisibility(View.GONE);
                }
            } else {
                imageView.setVisibility(View.GONE);
                upload_image.setVisibility(View.VISIBLE);

            }

        }
    }

    private void setActionbar() {

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Update Product");


    }

    @Override
    public boolean onSupportNavigateUp() { // handle back button in actionbar
        onBackPressed();
        return super.onSupportNavigateUp();

    }

    private boolean checkValidation() {
        boolean isValidate = false;
        if (product_name.getEditText().getText().toString().equals("")) {
            isValidate = false;
            showSnackBar(product_name, "Please Enter Product Name");
        } else if (product_regular_price.getEditText().getText().toString().equals("")) {
            isValidate = false;
            showSnackBar(product_name, "Please Enter Regular Price");
        } else if (product_sale_price.getEditText().getText().toString().equals("")) {
            isValidate = false;
            showSnackBar(product_name, "Please Enter Sale Price");
        } else if (product_desc.getEditText().getText().toString().equals("")) {
            isValidate = false;
            showSnackBar(product_name, "Please Enter Production description");
        } else if (mBitmap == null) {
            isValidate = false;
            showSnackBar(product_name, "Please Select image");
        } else {

            isValidate = true;
        }

        return isValidate;
    }

    private void showSnackBar(View view, String message) {

        Snackbar.make(view, message, Snackbar.LENGTH_SHORT).show();
    }

    private void setAvailableColors(List<Integer> color) {// set recyclerview with product available colors
        selectedColorsList = new ArrayList<>();
        for (int i = 0; i < color.size(); i++) {
            selectedColorsList.add(String.format("#%06X", (0xFFFFFF & color.get(i))));
        }
        color_recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        color_recyclerView.setAdapter(new AvailableRecyclerViewAdapter(color));


    }

    private void initializeViews() {

        imageView = (ImageView) findViewById(R.id.imageView);
        upload_image = (Button) findViewById(R.id.upload_image);
        product_name = (TextInputLayout) findViewById(R.id.product_name);
        product_regular_price = (TextInputLayout) findViewById(R.id.product_regular_price);
        product_sale_price = (TextInputLayout) findViewById(R.id.product_sale_price);
        product_desc = (TextInputLayout) findViewById(R.id.product_desc);
        update_product = (Button) findViewById(R.id.update_product);
        color_recyclerView = (RecyclerView) findViewById(R.id.color_recyclerView);
        bundle = getIntent().getExtras();

        if (bundle != null) {
            productId = bundle.getInt(Constants.PRODUCT_ID);
        }
        updateProductViewModel = ViewModelProviders.of(this).get(UpdateProductViewModel.class);// intialize viewmodel


        productLiveData = updateProductViewModel.getProduct(productId);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_GALLERY_PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) { // storage permission granted from here
                openGallery();
            } else {
                Toast.makeText(this, "Storage permission denied", Toast.LENGTH_LONG).show();
            }
        }
    }
}
