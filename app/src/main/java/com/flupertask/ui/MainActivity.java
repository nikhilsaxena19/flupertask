package com.flupertask.ui;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.flupertask.R;
import com.flupertask.adapter.ProductListAdapter;
import com.flupertask.model.Product;
import com.flupertask.utils.Constants;
import com.flupertask.viewmodel.ProductViewModel;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements ProductListAdapter.OnDeleteClickListener {

    private static final int NEW_PRODUCT_REQUEST_CODE = 1;
    public static final int UPDATE_NOTE_ACTIVITY_REQUEST_CODE = 2;
    private String TAG = this.getClass().getSimpleName();
    private ProductViewModel productViewModel;
    private ProductListAdapter productListAdapter;
    private Button show_product, create_product;
    private List<Integer> selectedColors = new ArrayList<>();
    private RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        create_product = (Button) findViewById(R.id.create_product);
        show_product = (Button) findViewById(R.id.show_product);

         recyclerView = findViewById(R.id.recyclerview);
        productListAdapter = new ProductListAdapter(this, this, getFragmentManager());
        recyclerView.setAdapter(productListAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        create_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, AddProductActivity.class); // for adding new product
                startActivityForResult(intent, NEW_PRODUCT_REQUEST_CODE);
            }
        });

        show_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getAllProducts(); // method for displaying current products in database
            }
        });
        productViewModel = ViewModelProviders.of(this).get(ProductViewModel.class); // viewmodel initialization


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == NEW_PRODUCT_REQUEST_CODE && resultCode == RESULT_OK) {

            // Code to insert Product

            Product product = new Product();
            product.setDescription(data.getStringExtra(Constants.PRODUCT_DESC));
            product.setName(data.getStringExtra(Constants.PRODUCT_NAME));
            product.setRegularPrice(data.getStringExtra(Constants.PRODUCT_REG_PRICE));
            product.setSalePrice(data.getStringExtra(Constants.PRODUCT_SALE_PRICE));
            product.setProductImage(data.getByteArrayExtra(Constants.PRODUCT_IMAGE));
            selectedColors.clear();
            for (int i = 0; i < data.getStringArrayListExtra(Constants.PRODUCT_COLORS).size(); i++) {
                selectedColors.add(Color.parseColor(data.getStringArrayListExtra(Constants.PRODUCT_COLORS).get(i)));
            } // for storing multiple colors in database

            product.setColor(selectedColors);
            productViewModel.insert(product); // method for inserting new product in database

            Toast.makeText(
                    getApplicationContext(),
                    R.string.saved,
                    Toast.LENGTH_LONG).show();
        } else if (requestCode == UPDATE_NOTE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {

            // Code to update the Product
            Product product = new Product();
            product.setId(Integer.parseInt(data.getStringExtra(Constants.PRODUCT_ID)));
            product.setDescription(data.getStringExtra(Constants.PRODUCT_DESC));
            product.setName(data.getStringExtra(Constants.PRODUCT_NAME));
            product.setRegularPrice(data.getStringExtra(Constants.PRODUCT_REG_PRICE));
            product.setSalePrice(data.getStringExtra(Constants.PRODUCT_SALE_PRICE));
            product.setProductImage(data.getByteArrayExtra(Constants.PRODUCT_IMAGE));
            selectedColors.clear();
            for (int i = 0; i < data.getStringArrayListExtra(Constants.PRODUCT_COLORS).size(); i++) {
                selectedColors.add(Color.parseColor(data.getStringArrayListExtra(Constants.PRODUCT_COLORS).get(i)));
            } // for storing multiple colors in database
            product.setColor(selectedColors);
            productViewModel.update(product);// method for updating product in database.

            Toast.makeText(
                    getApplicationContext(),
                    R.string.updated,
                    Toast.LENGTH_LONG).show();

        } else {
            Toast.makeText(
                    getApplicationContext(),
                    R.string.not_saved,
                    Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void OnDeleteClickListener(Product product) {
        // Code for Delete operation
        productViewModel.delete(product); // Method for deleting product from database

    }

    private void getAllProducts() {

        productViewModel.getAllProducts().observe(this, new Observer<List<Product>>() {
            @Override
            public void onChanged(@Nullable List<Product> products) {
                // will update automatically as data changes in database.

                productListAdapter.setProducts(products);
            }
        });
    }


}
